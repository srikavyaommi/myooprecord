//parent class
class P{
    int a=1;
    void display1(){
        System.out.println("Content in P is : "+a);
    }
}
//simple inheritance
class Q extends P{
    int b=2;
    void display2(){
        System.out.println("Content in  is Q: "+(a+b));
    }
}
//multilevel inheritance
class R extends Q{
    int c=3;
    void display3(){
        System.out.println("Content in R is : "+(b+c));
    }
}
//hierarchical inheritance
class S extends P{
    int d=4;
    void display4(){
        System.out.println("Content in S is : "+(a+d));
    }
}
//hybrid inheritance
class T extends Q{
    int e=5;
    void display5(){
        System.out.println("Content in T is : "+(b+e));
    }
}
class TypInh{
    public static void main(String[] args) {
        Q obj2=new Q();//Simple inheritance
        obj2.display2();
        R obj3=new R();//Multilevel inheritance
        obj3.display3();
        S obj4=new S();//Hierarchial inheritance
        obj4.display4();
        T obj5=new T();//Hybrid inheritance
        obj5.display5();
    }
}