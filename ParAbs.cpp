#include <iostream>
using namespace std;
class Abs{
    private:
    string x, y;
    public:
    void set(string a, string b){
        x = a;
        y = b;
    }
    void print(){
        cout<<"x is : " << x << endl;
        cout<<"y is : " << y << endl;
    }
};
int main(){
    Abs t1;
    t1.set("string a", "string b");
    t1.print();
    return 0;
}