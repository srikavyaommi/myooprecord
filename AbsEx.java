interface Person{
    void display();
}
class Student implements Person{
    public void display() {
        System.out.println("This is display method of the Student class");
    }
}
class Lecturer implements Person{
    public void display() {
        System.out.println("This is display method of the Lecturer class");
    }
}
public class AbsEx{
    public static void main(String args[]) {
        Person p1 = new Student();
        p1.display();
        Person p2 = new Lecturer();
        p2.display();
    }
}