public class Boxunbox{
    public static void main(String[] args) {
        int i = 143;
        Integer integer = Integer.valueOf(i);
        System.out.println("Boxed integer value is : " + integer);
        Double d = Double.valueOf(1.141);
        double x = d.doubleValue();
    System.out.println("Unboxed double value is : " + x);
    }
}