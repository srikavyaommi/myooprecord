#include<iostream>
using namespace std;
class Complex{
    int real,imag;
    public:
    Complex(int r=0,int i=0){
        real=r;
        imag=i;
    }
    Complex operator*(Complex const &obj){
        Complex res;
        res.real=real * obj.real;
        res.imag=imag * obj.imag;
        return res;
    }
    void display(){
        cout<<real<<" + i"<<imag<<endl;
    }
    
};
int main(){
Complex obj1(1,3);
Complex obj2(2,5);
Complex obj3 = obj1*obj2;
obj3.display();
}